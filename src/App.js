import React from "react";
import './App.css';
import logo from './logo.svg';
import ComponentName from "./ComponentName";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ComponentName />
        <ComponentName />
      </header>
    </div>
  );
}

export default App;
